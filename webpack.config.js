var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {

  entry: {
    app: './server.js',
    // no efffect - direct reference in html was necessary - jumbotron, carousel..
    carousel_style: './jumbotron.css',
    // @import in css was key, this was not effective...
    bootstrap_script: './node_modules/bootstrap/dist/js/bootstrap.min.js',
    bootstrap_style: './node_modules/bootstrap/dist/css/bootstrap.min.css',    
    bootstrap_theme_style: './node_modules/bootstrap/dist/css/bootstrap-theme.min.css'   
  },
  
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  
  // silence errors for packages that are not referenced
  node: {
      net: 'empty',
      fs: 'empty'
  },
  
  // add loaders as per warning/error feedback
  module : {
	  loaders : [
	      // Support for *.<extenion> files.
	      { test: /\.json$/,  loader: 'json' },
	      { test: /\.css$/,  loader: 'css' },

	      { test: /bootstrap\/js\//, loader: 'imports?jQuery=jquery' },
	      
	      { test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)$/, loader: 'url-loader?limit=10000' }, 
	      { test: /\.(eot|ttf|wav|mp3)$/, loader: 'file-loader' },
	      //{ test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader") }
	      //{ test: /\.css$/, loader: "style-loader!css-loader" }
    ]

  },
  plugins: [
      new ExtractTextPlugin("./jumbotron.css")
  ]
};